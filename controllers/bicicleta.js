var Bicicleta= require('../models/bicicleta');

exports.bicicleta_list=function(req, res){
    Bicicleta.allBicis((err, usuarios) => {
        res.render('bicicletas/index', {bicis :usuarios});
    });
}

exports.bicicleta_create_get=function(req, res){
    res.render('bicicletas/create');
}

exports.bicicleta_create_post=function(req, res){
    var aBici = {
        "code": req.body.id,
        "color": req.body.color,
        "modelo": req.body.modelo,
        "ubicacion": [req.body.lat,req.body.lng]
    };
    Bicicleta.add(aBici,(err) => {
        if (err) return console.log(err);
        console.log('bicicleta created')
        res.redirect('/bicicletas');
    });
}

exports.bicicleta_update_get=function(req, res, next){
  Bicicleta.findById(req.params.id, function(err, bici){
    res.render('bicicletas/update', {errors:{},bici:bici});
  });
}

exports.bicicleta_update_post=function(req, res){
     var update_values= {id: req.body.id,color: req.body.color, modelo:req.body.modelo,ubicacion:[req.body.lat, req.body.lng] };
     Bicicleta.findByIdAndUpdate(req.params.id, update_values, function(err, usuario){
        if (err){
            console.log(err);
            res.render('bicicletas/update', {errors: err.errors, usuario: new Bicicleta({id: req.body.id,color: req.body.color, modelo:req.body.modelo,ubicacion:[req.body.lat, req.body.lng]})});
          }else{
             res.redirect('/bicicletas');
             return;
            }
     });
}

exports.bicicleta_view_get=function(req, res, next){
    Bicicleta.findById(req.params.id, function(err, bici){
        res.render('bicicletas/view', {errors:{},bici:bici});
      });
}

exports.bicicleta_delete_post= function(req,res){
    Bicicleta.findByIdAndDelete(req.body.id,function(err){
        if (err)
            next(err);
            else
            res.redirect('/bicicletas');
    });   
}