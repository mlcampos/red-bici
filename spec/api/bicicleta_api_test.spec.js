var mongoose= require('mongoose');
var Bicicleta = require('../../models/bicicleta');
var request = require('request');
var server = require('../../bin/www');

var base_url= "http://localhost:5000/api/bicicletas";

beforeEach(function(){console.log('testeando…'); Bicicleta.allBicis = [];} );

describe('Bicicleta API', () => {
   beforeAll((done) => { mongoose.connection.close(done) });
   beforeEach(function(done){
        let mongoDB = 'mongodb://localhost/testdb';
        mongoose.connect(mongoDB, { useCreateIndex: true, useNewUrlParser: true, useUnifiedTopology: true});
        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection error'));
        db.once('open', function(){
        console.log('We are connected to test database!');
        done();
        });  
   });
   afterEach(function(done) {
        Bicicleta.deleteMany({}, function(err, success){
        if(err) console.log(err);
        done();
        mongoose.disconnect();
        });
    }); 

    describe(' GET BICICLETAS /', () => {
        it('Status 200', (done) => {
            
           Bicicleta.find({}, ( err, bicis ) => {
                expect(bicis.length).toBe(0);
            });

            // Añadir una bicicleta
            var aBici = new Bicicleta({code:1, color:"verde", modelo:"urbana"});
            Bicicleta.add(aBici, ( err, newBici) => {
                if (err) console.log(err);
            });
            
            request.get(base_url, (error, response, body) => {
                expect(response.statusCode).toBe(200);
                done();
            });
        });
    });

    describe(' POST BICICLETAS /create', () => {
        it('Status 200', (done) => {
            
            var headers = {'content-type' : 'application/json'};
            
            var aBici = '{"code":1, "color":"azul", "modelo":"Urbano", "lat":"10.974273", "lng":"-74.815885"}';

            request.post({
                headers : headers,
                url : base_url + '/create',
                body : aBici
            },
            function(error, response, body) {
                expect(response.statusCode).toBe(200);
                Bicicleta.findByCode(1, ( err, bicicleta) => {
                    if ( err) console.log(err);
                    expect(bicicleta.code).toBe(1);
                    done();
                });
                
            });
        });
    });


 /*   describe('POST BICICLETAS /delete', () => {
        it('Status 200', (done) => {

            var a = new Bicicleta(10,'violeta','urbana', [-31.419730, -64.188361]);
            var b = new Bicicleta(11,'azul','urbana', [-31.419730, -64.188361]);
            Bicicleta.add(a);
            Bicicleta.add(b);

            var headers = {'content-type' : 'application/json'};
            var aBici = '{ "id": 10}';
            request.delete({
                headers: headers,
                url:   base_url +  '/delete',
                body:   aBici
            }, function(error, response, body){
                  expect(response.statusCode).toBe(200);
                  //expect(Bicicleta.find({}).length).toBe(1);
                  done();
            });
        });
    });*/
});