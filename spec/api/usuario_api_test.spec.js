var mongoose = require('mongoose');
var Usuario = require('../../models/usuario');
var Bicicleta = require('../../models/bicicleta');
var Reserva = require('../../models/reserva');
var request = require('request');
var server = require('../../bin/www');

var base_url= "http://localhost:5000/api/usuarios";

describe('Testing Api Usuarios ', function() {
    beforeAll((done) => { mongoose.connection.close(done) });
    beforeEach(function(done){
        
        var mongoDB = "mongodb://localhost/testapidb";
        mongoose.connect(mongoDB, { useNewUrlParser:true, useUnifiedTopology: true, useCreateIndex: true, });

        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection error'));
        db.once('open', function(){
            console.log('We are connected to test database');
            done();
        });
    });

    afterEach(function(done){
        Reserva.deleteMany({}, function(err, success) {
            if (err) console.log(err);
            Usuario.deleteMany({}, function( err, success ){
                if (err) console.log(err);
                Bicicleta.deleteMany({}, function(err, success)  {
                    if (err) console.log(err);
                    done();
                    mongoose.disconnect();
                });
            });
        });
    });

    describe(' POST USUARIOS /create', () => {
        it('Status 200', (done) => {
            
            var headers = {'content-type' : 'application/json'};
            
            var aUser = '{"nombre":"Martin Arias"}';

            request.post({
                headers : headers,
                url : base_url + '/create',
                body : aUser
            },
            function(error, response, body) {
                expect(response.statusCode).toBe(200);
                Usuario.find({ "nombre":"Martin Arias" }, ( err, usuario) => {
                    if ( err) console.log(err);
                    //console.log(usuario);
                    expect(usuario[0].nombre).toEqual('Martin Arias');
                    done();
                });
                
            });
        
        });
    });

    describe(' Reservas /reservar', () => {
        it('devuelve la información de la reserva', (done) => {
            
            let usuario = new Usuario({"nombre": "Emanuel Guillen"});
            usuario.save();

            let bicicleta = new Bicicleta({"code": 1, "color":"azul","modelo":"urbano"});
            bicicleta.save();

            var headers = {'content-type' : 'application/json'};
            
            let aReserva = {
                id: usuario._id,
                bici_id: bicicleta._id,
                desde: "2020-08-03",
                hasta: "2020-08-05"
            };

            request.post({
                headers : headers,
                url : base_url + '/reservar',
                body : JSON.stringify(aReserva)
            },
            function(error, response, body) {
                expect(response.statusCode).toBe(200);
                Reserva.find({}).populate('bicicleta').populate('usuario').exec(function(err, reservas) {
                    if ( err) console.log(err);
                    done();
                });
                
            });
        
        });
    });
});